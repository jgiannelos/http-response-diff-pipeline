.PHONY: prepare-dir prepare-db start-taskrunner spawn-tasks tear-down

prepare-dir:
	mkdir -p pgdata
prepare-db: prepare-dir
	docker-compose up -d --wait db broker
	docker compose exec db bash -c "psql -U dbuser difftesting -c 'CREATE SCHEMA difftesting'"
start-taskrunner: prepare-db
	docker compose up -d taskrunner flower
spawn-tasks: start-taskrunner
	docker compose run taskrunner difftesting-start-pipeline
tear-down:
	docker compose stop
	docker compose rm -f
	rm -rf pgdata