# HTTP responses diffing pipeline

## Description

A lightweight celery based pipeline to diff responses from two endpoints.
Includes helpers for WMF specific functionality.

### Use case
This tool has been very useful when there is a need to create a semantic diff of two HTTP endpoints
that serve a tree like output (eg. HTML, JSON).

### Features

* Supports a flexible way to give input
  * CSV based
  * Each testcase is described by a template URL that can be populated using the column data from input
* Scales horizontally using a distributed task based architecture
  * Task workers can be scaled up/down depending on needs
* Provides insights on the task and task workers on each run
  * Celery monitoring backend: `http://localhost:5555`
* Provides a web report for each run
  * Panel report backend: `http://localhost:5556`

## Requirements

- Docker
- Docker compose

## Config

Project uses the following config files:

- config/celery.yaml
  - Celery setup specific config
- config/pipeline.yaml
  - Difftesting pipeline specific config
    - endpoint templates
    - report backend
- config/targets.csv
  - CSV with scopes for target endpoints

### Example setup

```
± cat config/celery.yaml
broker_url: "redis://broker:6379/0"
result_backend: "db+postgresql://dbuser:dbpass@db/difftesting"

± cat config/pipeline.yaml
url_template_A: "https://{{domain}}/api/rest_v1/page/html/{{title}}"
url_template_B: "https://{{domain}}/w/rest.php/v1/page/{{title}}/html"
content_type: "text/html"
targets_path: "/config/targets.csv"
result_db_url: "postgresql://dbuser:dbpass@db/difftesting"

± cat config/targets.csv
domain,title
en.wikipedia.org,Dog
en.wikipedia.org,Cat%
```

To prepare the local setup run the following

```
> echo "UID=$(id -u)\nGID=$(id -g)" >> .env
> mkdir pgdata
> docker compose up -d broker db
> docker compose exec db bash
> psql -U dbuser difftesting

difftesting=# CREATE SCHEMA difftesting;
difftesting=# \q

> exit
```

To run the difftesting pipeline

```
> docker compose up -d taskrunner
> docker compose run taskrunner difftesting-start-pipeline
```

To run the report frontend:

```
> docker compose up -d report
> Visit localhost:5556
```

To orchestrate a run with a single command:

```
make spawn-tasks
```

To tear-down environment and data (WARNING this is a destructive operation):

```
make tear-down
```