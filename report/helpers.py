import functools

import panel

from http_response_diff.lib.report import get_result_by_id, get_results_dataframe


def rowDiffViewer(dbUri, diffAttr, row):
    obj = get_result_by_id(dbUri, int(row["id"]))
    diff = panel.widgets.Terminal(sizing_mode="stretch_width")
    diff.write(getattr(obj, diffAttr))
    return panel.Row(diff)


def getDiffTable(dbUri, diffAttr):
    return panel.widgets.Tabulator(
        get_results_dataframe(dbUri),
        page_size=50,
        disabled=True,
        row_content=functools.partial(rowDiffViewer, dbUri, diffAttr),
        hidden_columns=["index"]
    )
