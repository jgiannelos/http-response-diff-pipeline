import os

import panel
from helpers import getDiffTable

panel.extension("tabulator")
panel.extension("terminal")

DB_URI = os.environ.get("DB_URI")

table = getDiffTable(DB_URI, "headers_diff")
template = panel.template.BootstrapTemplate(title="HTTP headers diff viewer")
template.main.append(panel.Row(table))

template.servable()
