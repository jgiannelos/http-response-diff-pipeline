import os

import pandas
import panel
import plotly.express as px

from http_response_diff.lib.report import get_results_dataframe

DB_URI = os.environ.get("DB_URI")

panel.extension("plotly")

df = get_results_dataframe(DB_URI)
df["latency_change"] = 100 * (df["b_latency"] - df["a_latency"]) / df["a_latency"]

# Compute latency metrics for dataviz
latency_quantiles = pandas.DataFrame(
    df["latency_change"].quantile(q=[0.1, 0.25, 0.5, 0.75, 0.90, 0.95, 0.99])
)
latency_quantiles["percentiles"] = 100 * latency_quantiles.index
latency_fig = px.bar(
    latency_quantiles, title="Latency change", x="percentiles", y="latency_change"
)
latency_fig.update_layout(
    xaxis_title="Percentile (%)", yaxis_title="Latency change (%)"
)

# Compute headers diff metrics for dataviz
headers_diff_quantiles = pandas.DataFrame(
    df["headers_diff_len"].quantile(q=[0.1, 0.25, 0.5, 0.75, 0.90, 0.95, 0.99])
)
headers_diff_quantiles["percentiles"] = 100 * headers_diff_quantiles.index
headers_fig = px.bar(
    headers_diff_quantiles,
    title="Headers changed",
    x="percentiles",
    y="headers_diff_len",
)
headers_fig.update_layout(xaxis_title="Percentile (%)", yaxis_title="Headers changed")

# Compute content diff metrics for dataviz
content_diff_quantiles = pandas.DataFrame(
    df["content_diff_len"].quantile(q=[0.1, 0.25, 0.5, 0.75, 0.90, 0.95, 0.99])
)
content_diff_quantiles["percentiles"] = 100 * content_diff_quantiles.index
content_fig = px.bar(
    content_diff_quantiles,
    title="Content entries changed",
    x="percentiles",
    y="content_diff_len",
)
content_fig.update_layout(
    xaxis_title="Percentile (%)", yaxis_title="Content entries changed"
)

# Render panels
template = panel.template.BootstrapTemplate(title="Overview")
template.main.append(
    panel.Row(panel.pane.Plotly(latency_fig)),
)
template.main.append(
    panel.Row(panel.pane.Plotly(headers_fig)),
)
template.main.append(
    panel.Row(panel.pane.Plotly(content_fig)),
)

template.servable()
