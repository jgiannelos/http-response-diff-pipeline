FROM python:3.10

ARG UID=1000
ARG GID=1000
ARG DIFFT_RELEASE=https://github.com/Wilfred/difftastic/releases/download/0.62.0/difft-x86_64-unknown-linux-gnu.tar.gz

COPY . /app
WORKDIR /app

USER root
RUN apt-get update && apt-get install -y libpq-dev wget
RUN wget -O /tmp/difft.tar.gz ${DIFFT_RELEASE}
RUN tar xvf /tmp/difft.tar.gz -C /usr/local/bin

RUN pip install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction

RUN groupadd -g "${GID}" python && useradd --create-home --no-log-init -u "${UID}" -g "${GID}" python
USER python