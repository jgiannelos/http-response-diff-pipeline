import unittest

import jinja2
import respx

from http_response_diff.lib import helpers


class TestHelpers(unittest.TestCase):
    def test_get_random_page(self):
        domain = "en.wikipedia.org"
        ns = 0
        random = helpers.get_random_page(domain, ns)
        self.assertCountEqual(random.keys(), ["id", "ns", "title"])

    def test_get_random_pages(self):
        domain = "en.wikipedia.org"
        ns = 0
        limit = 10
        random_pages = helpers.get_random_pages(domain, limit, ns)
        self.assertEqual(len(random_pages), limit)
        for page in random_pages:
            self.assertCountEqual(page.keys(), ["id", "ns", "title"])

    def test_get_response_from_url_template(self):
        env = jinja2.Environment()
        template = env.from_string("https://{{ domain }}/{{ path }}")
        scope = {"domain": "en.wikipedia.org", "path": "api/rest_v1/page/html/Earth"}
        res = helpers.get_response_from_url_template(template, scope)
        self.assertTrue(res.status_code >= 200 and res.status_code <= 300)
        self.assertEqual(
            str(res.request.url), "https://en.wikipedia.org/api/rest_v1/page/html/Earth"
        )

    @respx.mock
    def test_diffg_response_html(self):
        respx.get("https://example.com/a").respond(
            html="<html><head><title>A</title></head><body><h1>A</h1></body></html>"
        )
        respx.get("https://example.com/b").respond(
            html="<html><head><title>B</title></head><body><h1>B</h1></body></html>"
        )
        env = jinja2.Environment()
        template = env.from_string("https://{{ domain }}/{{ page }}")
        resA = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "a"}
        )
        resB = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "b"}
        )
        diff = helpers.diffg_response_html(resA, resB)
        self.assertTrue(len(diff) > 0)

    @respx.mock
    def test_diffg_response_json(self):
        respx.get("https://example.com/a").respond(json={"a": 1, "b": 2})
        respx.get("https://example.com/b").respond(json={"a": 1, "b": 3})
        env = jinja2.Environment()
        template = env.from_string("https://{{ domain }}/{{ page }}")
        resA = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "a"}
        )
        resB = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "b"}
        )
        diff = helpers.diffg_response_json(resA, resB)
        self.assertTrue(len(diff) > 0)

    @respx.mock
    def test_diffg_response_header(self):
        respx.get("https://example.com/a").respond(headers={"a": "1", "b": "2"})
        respx.get("https://example.com/b").respond(headers={"a": "1", "b": "3"})
        env = jinja2.Environment()
        template = env.from_string("https://{{ domain }}/{{ page }}")
        resA = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "a"}
        )
        resB = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "b"}
        )
        diff = helpers.diffg_response_headers(resA, resB)
        self.assertTrue(len(diff) > 0)

    @respx.mock
    def test_difft_response_html(self):
        respx.get("https://example.com/a").respond(
            html="<html><head><title>A</title></head><body><h1>A</h1></body></html>"
        )
        respx.get("https://example.com/b").respond(
            html="<html><head><title>B</title></head><body><h1>B</h1></body></html>"
        )
        env = jinja2.Environment()
        template = env.from_string("https://{{ domain }}/{{ page }}")
        resA = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "a"}
        )
        resB = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "b"}
        )
        diff = helpers.difft_response_html(resA, resB)
        self.assertTrue(len(diff) > 0)

    @respx.mock
    def test_difft_response_json(self):
        respx.get("https://example.com/a").respond(json={"a": 1, "b": 2})
        respx.get("https://example.com/b").respond(json={"a": 1, "b": 3})
        env = jinja2.Environment()
        template = env.from_string("https://{{ domain }}/{{ page }}")
        resA = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "a"}
        )
        resB = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "b"}
        )
        diff = helpers.difft_response_json(resA, resB)
        self.assertTrue(len(diff) > 0)

    @respx.mock
    def test_difft_response_header(self):
        respx.get("https://example.com/a").respond(headers={"a": "1", "b": "2"})
        respx.get("https://example.com/b").respond(headers={"a": "1", "b": "3"})
        env = jinja2.Environment()
        template = env.from_string("https://{{ domain }}/{{ page }}")
        resA = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "a"}
        )
        resB = helpers.get_response_from_url_template(
            template, {"domain": "example.com", "page": "b"}
        )
        diff = helpers.difft_response_headers(resA, resB)
        self.assertTrue(len(diff) > 0)
