import datetime
import io
import json
import random
import tempfile
import xml.etree.ElementTree as ET

import graphtage
import httpx
import jinja2
import sh
from bs4 import BeautifulSoup


def get_random_pages(domain: str, limit: int, namespace: int) -> list[dict]:
    """Fetches N amount of random pages for a given wiki domain and namespace"""
    url = f"https://{domain}/w/api.php"
    params = {
        "action": "query",
        "format": "json",
        "list": "random",
        "rnlimit": limit,
        "rnnamespace": namespace,
    }
    res = httpx.get(url, params=params, follow_redirects=True)
    res.raise_for_status()
    random_pages = res.json()
    return random_pages["query"]["random"]


def get_random_page(domain: str, namespace: int) -> dict:
    """Get a single random page for a given wiki domain and namespace"""
    return get_random_pages(domain, 1, namespace)[0]


def get_random_date(
    start_year: int, end_year: int, start_month=1, start_day=1, end_month=12, end_day=31
) -> datetime.date:
    """Get a random date between start/end year/month/day"""
    start = datetime.date(start_year, start_month, start_day)
    end = datetime.date(end_year, end_month, end_day)
    diff = (end - start).days
    return start + datetime.timedelta(random.randint(0, diff))


def get_fmt_date(d: datetime.date, fmt: str) -> str:
    """Get a date formatted to the given format"""
    return d.strftime(fmt)


def get_template_from_string(template_str: str) -> jinja2.Template:
    """Builds a jinja2 template from string"""
    env = jinja2.Environment()
    custom_funcs = {
        "diff_pipeline": {
            "random_mw_article": get_random_pages,
            "random_date": get_random_date,
            "format_date": get_fmt_date,
        }
    }
    template = env.from_string(template_str)
    template.globals.update(custom_funcs)
    return template


def get_response_from_url_template(
    template: jinja2.Template,
    scope: dict,
    verify=True,
    headers: httpx.Headers = {},
    timeout: float = 5,
) -> httpx.Response:
    """GETs a response from URL based on a template and scope"""
    url = template.render(scope)
    res = httpx.get(
        url, follow_redirects=True, verify=verify, headers=headers, timeout=timeout
    )
    res.raise_for_status()
    return res


def get_response_json_from_url_template(
    template: jinja2.Template,
    scope: dict,
    verify=True,
    headers: httpx.Headers = {},
    timeout: float = 5,
) -> httpx.Response:
    """GETs a response from URL based on a template and parses json"""
    res = get_response_from_url_template(template, scope, verify, headers, timeout)
    return res.json()


def get_diff_digest(from_tree, to_tree, formatter) -> str:
    """Get diff digest between two trees"""
    out = io.StringIO()
    printer = graphtage.printer.Printer(out, quiet=True, ansi_color=False)
    for ancestors, edit in from_tree.get_all_edit_contexts(to_tree):
        for i, node in enumerate(ancestors):
            if node.parent is not None:
                node.parent.print_parent_context(printer, for_child=node)
            if i == len(ancestors) - 1:
                printer.write(" -> ")
                formatter.print(printer, edit)
        printer.newline()
    return out.getvalue()


BUILD_OPTIONS = graphtage.BuildOptions(
    allow_key_edits=False,
    auto_match_keys=False,
    allow_list_edits=False,
    allow_list_edits_when_same_length=False,
)


def diffg_response_headers(a: httpx.Response, b: httpx.Response) -> str:
    """Diff the headers between two responses using graphtage"""
    formatter = graphtage.pydiff.PyDiffFormatter.DEFAULT_INSTANCE
    from_tree = graphtage.pydiff.build_tree(dict(a.headers), options=BUILD_OPTIONS)
    to_tree = graphtage.pydiff.build_tree(dict(b.headers), options=BUILD_OPTIONS)
    return get_diff_digest(from_tree, to_tree, formatter)


def diffg_response_json(a: httpx.Response, b: httpx.Response) -> str:
    """Diff the JSON between two responses using graphtage"""
    formatter = graphtage.json.JSONFormatter.DEFAULT_INSTANCE
    from_tree = graphtage.json.build_tree(a.json(), options=BUILD_OPTIONS)
    to_tree = graphtage.json.build_tree(b.json(), options=BUILD_OPTIONS)
    return get_diff_digest(from_tree, to_tree, formatter)


def diffg_response_html(a: httpx.Response, b: httpx.Response) -> str:
    """Diff the HTML text between two responses using graphtage"""
    formatter = graphtage.xml.XMLFormatter.DEFAULT_INSTANCE
    a_pretty = BeautifulSoup(a.text, "lxml").prettify()
    b_pretty = BeautifulSoup(b.text, "lxml").prettify()

    treeA = ET.fromstring(a_pretty)
    treeB = ET.fromstring(b_pretty)
    filetype = graphtage.get_filetype(mime_type="text/html")
    from_tree = filetype.build_tree(treeA, options=BUILD_OPTIONS)
    to_tree = filetype.build_tree(treeB, options=BUILD_OPTIONS)
    return get_diff_digest(from_tree, to_tree, formatter)


def diffg_response_content(
    a: httpx.Response, b: httpx.Response, content_type: str
) -> str:
    """Diff the content between two responses using difftastic"""

    differ = {
        "application/json": diffg_response_json,
        "text/html": diffg_response_html,
    }

    return differ[content_type](a, b)


def difft(a: str, b: str, file_extension: str) -> str:
    """Diff content using difftastic subcommand"""
    temp_path = tempfile.mkdtemp()
    a_path = f"{temp_path}/a.{file_extension}"
    b_path = f"{temp_path}/b.{file_extension}"

    with open(a_path, "w") as f:
        f.write(a)

    with open(b_path, "w") as f:
        f.write(b)

    return sh.difft(a_path, b_path, "--context", 0, _tty_size=(180, 180))


def difft_response_headers(a: httpx.Response, b: httpx.Response) -> str:
    """Diff the headers between two responses using difftastic"""
    return difft(
        json.dumps(dict(a.headers), indent=2),
        json.dumps(dict(b.headers), indent=2),
        "json",
    )


def difft_response_json(a: httpx.Response, b: httpx.Response) -> str:
    """Diff the content between two JSON responses using difftastic"""
    return difft(json.dumps(a.text, indent=2), json.dumps(b.text, indent=2), "json")


def difft_response_html(a: httpx.Response, b: httpx.Response) -> str:
    """Diff the content between two HTML responses using difftastic"""
    a_pretty = BeautifulSoup(a.text, "lxml").prettify()
    b_pretty = BeautifulSoup(b.text, "lxml").prettify()
    return difft(a_pretty, b_pretty, "html")


def difft_response_content(
    a: httpx.Response, b: httpx.Response, content_type: str
) -> str:
    """Diff the content between two responses using difftastic"""

    differ = {
        "application/json": difft_response_json,
        "text/html": difft_response_html,
    }

    return differ[content_type](a, b)
