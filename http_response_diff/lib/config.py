from dataclasses import dataclass, field
from typing import List

from omegaconf import MISSING, OmegaConf


@dataclass
class PipelineConfig:
    url_template_A: str = MISSING
    url_template_B: str = MISSING
    content_type: str = MISSING
    targets_path: str = MISSING
    verify_TLS: bool = True
    result_db_url: str = MISSING
    headers_A: dict = field(default_factory=lambda: {})
    headers_B: dict = field(default_factory=lambda: {})
    http_timeout: float = 60
    diffing_backend: str = "graphtage"


@dataclass
class CeleryConfig:
    broker_url: str = MISSING
    result_backend: str = MISSING
    task_serializer: str = "pickle"
    result_serializer: str = "pickle"
    event_serializer: str = "pickle"
    accept_content: List[str] = field(default_factory=lambda: ["pickle", "json"])
    result_accept_content: List[str] = field(default_factory=lambda: ["pickle", "json"])
    task_soft_time_limit: int = 300
    result_expires: int = 0


def get_pipeline_config(path):
    yaml_conf = OmegaConf.load(path)
    schema = OmegaConf.structured(PipelineConfig)
    config = OmegaConf.merge(schema, yaml_conf)
    return config


def get_celery_config(path):
    yaml_conf = OmegaConf.load(path)
    schema = OmegaConf.structured(CeleryConfig)
    config = OmegaConf.merge(schema, yaml_conf)
    return config
