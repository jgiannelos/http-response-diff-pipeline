import datetime
import json

import httpx
import pandas as pd
import sqlalchemy
import sqlalchemy.orm
from sqlalchemy import DateTime
from sqlalchemy.orm import Mapped, mapped_column

from http_response_diff.lib import helpers


class Base(sqlalchemy.orm.DeclarativeBase):
    pass


class DifftestingResult(Base):
    """Diff testing result model"""

    __tablename__ = "difftesting_results"
    __table_args__ = {"schema": "difftesting"}

    id: Mapped[int] = mapped_column(primary_key=True)
    created: Mapped[datetime.datetime] = mapped_column(
        DateTime(timezone=True), default=datetime.datetime.now
    )

    a_url: Mapped[str]
    a_latency: Mapped[float]
    a_headers: Mapped[str]
    a_content: Mapped[str]
    a_status_code: Mapped[int]
    b_url: Mapped[str]
    b_latency: Mapped[float]
    b_headers: Mapped[str]
    b_content: Mapped[str]
    b_status_code: Mapped[int]
    headers_diff: Mapped[str]
    headers_diff_len: Mapped[int]
    content_diff: Mapped[str]
    content_diff_len: Mapped[int]


def init_db(db_url: str):
    """Initialize DB for diff testing results"""
    engine = sqlalchemy.create_engine(db_url)
    DifftestingResult.metadata.create_all(engine)


def process_results(
    resA: httpx.Response,
    resB: httpx.Response,
    content_type: str,
    diffing_backend: str = "graphtage",
):
    """Process results for diff testing"""
    differs = {
        "graphtage": {
            "headers": helpers.diffg_response_headers,
            "content": helpers.diffg_response_content,
        },
        "difftastic": {
            "headers": helpers.difft_response_headers,
            "content": helpers.difft_response_content,
        },
    }
    differ = differs[diffing_backend]
    headers_diff = differ["headers"](resA, resB)
    content_diff = differ["content"](resA, resB, content_type)
    return {
        "a_url": str(resA.url),
        "a_latency": resA.elapsed.total_seconds(),
        "a_headers": json.dumps(dict(resA.headers)),
        "a_content": resA.text,
        "a_status_code": resA.status_code,
        "b_url": str(resB.url),
        "b_latency": resB.elapsed.total_seconds(),
        "b_headers": json.dumps(dict(resB.headers)),
        "b_content": resB.text,
        "b_status_code": resB.status_code,
        "headers_diff": headers_diff,
        "headers_diff_len": len(headers_diff.split("\n")),
        "content_diff": content_diff,
        "content_diff_len": len(content_diff.split("\n")),
    }


def store_results(db_url: str, res: dict):
    """Store diff testing results entry to DB"""
    engine = sqlalchemy.create_engine(db_url, pool_size=1)
    with sqlalchemy.orm.Session(engine) as session:
        entry = DifftestingResult(**res)
        session.add(entry)
        session.commit()
        session.close()
    engine.dispose()


def get_results_dataframe(db_url: str):
    """Get results dataframe"""
    engine = sqlalchemy.create_engine(db_url)
    with sqlalchemy.orm.Session(engine):
        q = sqlalchemy.select(
            DifftestingResult.id,
            DifftestingResult.created,
            DifftestingResult.a_url,
            DifftestingResult.a_latency,
            DifftestingResult.a_status_code,
            DifftestingResult.b_url,
            DifftestingResult.b_latency,
            DifftestingResult.b_status_code,
            DifftestingResult.headers_diff_len,
            DifftestingResult.content_diff_len,
        )
        df = pd.read_sql(q, engine)
    return df


def get_result_by_id(db_url: str, id: int):
    """Get result object by id"""
    engine = sqlalchemy.create_engine(db_url)
    with sqlalchemy.orm.Session(engine) as session:
        q = session.query(DifftestingResult).filter(DifftestingResult.id == id)
        return q.first()
