import os

import pandas
from celery import Celery
from kombu.serialization import registry
from tqdm import tqdm

from http_response_diff.lib import config, helpers, report

app = Celery("difftesting")
registry.enable("pickle")
app.config_from_object(config.get_celery_config(os.environ["CELERY_CONFIG_PATH"]))


@app.task
def get_endpoint(template_str, scope, verify, headers={}, timeout=5):
    template = helpers.get_template_from_string(template_str)
    return helpers.get_response_from_url_template(
        template, scope, verify, headers=headers, timeout=timeout
    )


@app.task
def process_results(responses, db_url, content_type):
    resA = responses[0]
    resB = responses[1]
    report_result = report.process_results(resA, resB, content_type)
    report.store_results(db_url, report_result)


@app.task
def run_diff_testcase(pipeline_config, scope):
    responses = [
        get_endpoint(
            pipeline_config.url_template_A,
            scope,
            pipeline_config.verify_TLS,
            pipeline_config.headers_A,
            pipeline_config.http_timeout,
        ),
        get_endpoint(
            pipeline_config.url_template_B,
            scope,
            pipeline_config.verify_TLS,
            pipeline_config.headers_B,
            pipeline_config.http_timeout,
        ),
    ]
    process_results(
        responses, pipeline_config.result_db_url, pipeline_config.content_type
    )


def run_pipeline():
    pipeline_config = config.get_pipeline_config(os.environ["PIPELINE_CONFIG_PATH"])
    df = pandas.read_csv(pipeline_config.targets_path)

    report.init_db(pipeline_config.result_db_url)

    for scope in tqdm(df.to_dict("records")):
        task = run_diff_testcase.s(pipeline_config, scope)
        task.delay()
